package com.romaingk.product_service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.romaingk.product_service.dto.ProductRequest;
import com.romaingk.product_service.dto.ProductResponse;
import com.romaingk.product_service.model.Product;
import com.romaingk.product_service.repository.ProductRepository;
import com.romaingk.product_service.service.ProductService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.containers.MongoDBContainer;

import java.math.BigDecimal;
import java.util.List;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;



@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductRepository repository;
    @Container
    private static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:latest");
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Test
    void testThatCreateProductReturnHttpStatusCreate201() throws Exception {
        ProductRequest request = getProductRequest();
        String mapToString = objectMapper.writeValueAsString(request);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToString))
                .andExpect(status().isCreated());
    }

    @Test
    void testThatCreateProductSuccessfullyReturnsSavedProduct() throws Exception {
        ProductRequest request = getProductRequest();
        String mapToString = objectMapper.writeValueAsString(request);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToString))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(request.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(request.getPrice()));
    }

    @Test
    void testThatFindAllProductReturnHttpStatus200() throws Exception {
        mockMvc.perform(get("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    void testThatFindAllProductReturnProductInTheDatabase() throws Exception {
        // Préparer les données de test
        Product product1 = Product.builder()
                .id("1")
                .name("iPhone 15")
                .description("Last iPhone")
                .price(BigDecimal.valueOf(1500.0))
                .build();

        Product product2 = Product.builder()
                .id("2")
                .name("Samsung Galaxy S23")
                .description("Latest Samsung phone")
                .price(BigDecimal.valueOf(1300.0))
                .build();

        List<Product> productList = List.of(product1, product2);
        when(repository.findAll()).thenReturn(productList);

         mockMvc.perform(get("/api/product")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                 .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(product1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(product1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(product1.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].price").value(product1.getPrice().doubleValue()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(product2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(product2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].description").value(product2.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].price").value(product2.getPrice().doubleValue()));
    }

    private ProductRequest getProductRequest(){
        return ProductRequest.builder()
                .name("iphone 15")
                .description("last iphone")
                .price(BigDecimal.valueOf(1500))
                .build();
    }


}
