package com.romaingk.product_service.service;

import com.romaingk.product_service.dto.ProductRequest;
import com.romaingk.product_service.dto.ProductResponse;
import com.romaingk.product_service.model.Product;
import com.romaingk.product_service.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository repository;
    public ProductResponse createProduct(ProductRequest request) {
        Product product = Product.builder()
                .name(request.getName())
                .description(request.getDescription())
                .price(request.getPrice())
                .build();

        Product savedProduct = repository.save(product);
        log.info("Product {} is saved", savedProduct.getId());

        return mapProductToProductResponse(savedProduct);
    }

    public List<ProductResponse> getAllProduct() {
        List<Product> products = repository.findAll();
        return products.stream().map(this::mapProductToProductResponse).toList();

    }

    public ProductResponse mapProductToProductResponse(Product product){
       return ProductResponse.builder()
               .id(product.getId())
               .name(product.getName())
               .description(product.getDescription())
               .price(product.getPrice())
               .build();
    }

    public List<ProductResponse> getProductById(String id) {
        Optional<Product> product =  repository.findById(id);

        return  product.stream().map(this::mapProductToProductResponse).toList();
    }
}
