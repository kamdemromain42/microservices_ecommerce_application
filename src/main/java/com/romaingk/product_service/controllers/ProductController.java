package com.romaingk.product_service.controllers;

import com.romaingk.product_service.dto.ProductRequest;
import com.romaingk.product_service.dto.ProductResponse;
import com.romaingk.product_service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/product")
@RequiredArgsConstructor
public class ProductController {
    public final ProductService service;
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductResponse createProduct(@RequestBody ProductRequest request){
        return service.createProduct(request);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getAllProduct() {
        List<ProductResponse> products = service.getAllProduct();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductResponse> getProductById(@PathVariable("id") String id){
        return service.getProductById(id);
    }


}
